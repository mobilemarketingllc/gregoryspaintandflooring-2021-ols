var $ = jQuery;

jQuery(document).ready(function(){
    var getTopposition = jQuery('.fl-page-bar').outerHeight(true) + (parseInt(jQuery('header.fl-page-header-primary').outerHeight(true) / 4)) - 5;
    jQuery('#responsive-menu-button').css('top', getTopposition);
});